package database

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
	"strconv"
	"synack_challenge/internal/logger"
)

func NewClient() *redis.Client {
	addr := getRedisAddr()
	pw := getRedisPw()
	db := getRedisDB()
	c := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pw,
		DB:       db,
	})

	err := c.Ping(context.Background()).Err()
	if err != nil {
		panic(fmt.Sprintf("redis database can't be initialized. error: %s", err.Error()))
	}

	logger.Info("redis connection established")
	return c
}

func getRedisAddr() string {
	addr := os.Getenv("REDIS_ADDRESS")
	if addr == "" {
		logger.Info("Using default Redis address")
		addr = "localhost"
	}
	port := os.Getenv("REDIS_PORT")
	if port == "" {
		logger.Info("Using default Redis port")
		port = "6379"
	}
	return fmt.Sprintf("%s:%s", addr, port)
}

func getRedisPw() string {
	pw := os.Getenv("REDIS_PW")
	if pw == "" {
		logger.Info("Using default Redis pw")
		return pw
	}
	return pw
}

func getRedisDB() int {
	db := os.Getenv("REDIS_DB")
	if db == "" {
		logger.Info("Using default Redis DB")
		return 0
	}
	d, _ := strconv.Atoi(db)
	return d
}
