package database

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestGetRedisAddrShouldReturnDefaultAddr(t *testing.T) {
	require.Equal(t, "localhost:6379", getRedisAddr())
}

func TestGetRedisPwShouldReturnDefault(t *testing.T) {
	require.Equal(t, "", getRedisPw())
}

func TestGetRedisDBShouldReturnDefault(t *testing.T) {
	require.Equal(t, 0, getRedisDB())
}
