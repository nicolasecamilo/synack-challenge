package domain

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/logger"
)

type ImageInformation struct {
	Identifier       string                `json:"id"`
	Format           string                `json:"file_format"`
	Dimensions       ImageDimension        `json:"dimensions"`
	ColorComposition ImageColorComposition `json:"color_composition"`
	Status           ImageStatus           `json:"image_status"`
}

type ImageDimension struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type ImageColorComposition struct {
	Red   int `json:"R"`
	Green int `json:"G"`
	Blue  int `json:"B"`
}

type ImageStatus struct {
	Status Status  `json:"status"`
	Code   int     `json:"code"`
	Reason *string `json:"reason,omitempty"`
}

func (i *ImageInformation) ToString() (string, apierror.ApiError) {
	imgBytes, err := json.Marshal(i)
	if err != nil {
		msg := "error marshaling the struct"
		logger.Error(msg, zap.Error(err))
		return "", apierror.NewInternalServerError(msg, err)
	}

	return string(imgBytes), nil
}

func StringToImageData(data string) (*ImageInformation, apierror.ApiError) {
	var result ImageInformation
	err := json.Unmarshal([]byte(data), &result)
	if err != nil {
		msg := "error unmarshalling the data"
		logger.Error(msg, zap.Error(err))
		return nil, apierror.NewInternalServerError(msg, err)
	}

	return &result, nil
}

func (i *ImageStatus) SetStatusCode() {
	switch i.Status {
	case PROCESSING:
		i.Code = http.StatusProcessing
	case SUCCESS:
		i.Code = http.StatusCreated
	case FAILURE:
		i.Code = http.StatusInternalServerError
	}
}
