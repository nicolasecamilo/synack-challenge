package domain

type Status string

const (
	PROCESSING Status = "Processing"
	FAILURE    Status = "Failure"
	SUCCESS    Status = "Success"
)
