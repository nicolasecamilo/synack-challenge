package domain

import (
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestImageInformationToString(t *testing.T) {
	img := ImageInformation{
		Identifier: "test",
		Format:     "jpeg",
	}

	imgStr, err := img.ToString()
	require.Nil(t, err)
	require.Equal(t, `{"id":"test","file_format":"jpeg","dimensions":{"width":0,"height":0},"color_composition":{"R":0,"G":0,"B":0},"image_status":{"status":"","code":0}}`, imgStr)
}

func TestStringToImageData(t *testing.T) {
	str := `{"id":"test","file_format":"jpeg","dimensions":{"width":100,"height":100},"color_composition":{"R":0,"G":0,"B":0},"image_status":{"status":"","code":0}}`
	img, err := StringToImageData(str)
	require.Nil(t, err)
	require.Equal(t, 100, img.Dimensions.Height)
}

func TestSetStatusCode(t *testing.T) {
	img := ImageInformation{Status: ImageStatus{Status: PROCESSING}}
	img.Status.SetStatusCode()
	require.Equal(t, http.StatusProcessing, img.Status.Code)
	img.Status.Status = SUCCESS
	img.Status.SetStatusCode()
	require.Equal(t, http.StatusCreated, img.Status.Code)
	img.Status.Status = FAILURE
	img.Status.SetStatusCode()
	require.Equal(t, http.StatusInternalServerError, img.Status.Code)
}
