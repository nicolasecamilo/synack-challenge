package http

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"synack_challenge/internal/database"
	"synack_challenge/internal/images"
)

func MapRoutes(r *gin.Engine) {
	redisClient := database.NewClient()

	imageRepository := images.NewRepository(redisClient)
	imageService := images.NewService(imageRepository)
	imageHandler := images.NewHandler(imageService)

	r.GET("/ping", func(context *gin.Context) {
		context.JSON(http.StatusOK, "pong")
	})

	r.POST("/images", imageHandler.Post)
	r.GET("/images/:id", imageHandler.Get)
}
