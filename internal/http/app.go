package http

import (
	"github.com/gin-gonic/gin"
)

func InitApp() *gin.Engine {
	router := setupRouter()
	MapRoutes(router)
	return router
}

func setupRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Recovery())
	return r
}
