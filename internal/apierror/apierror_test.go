package apierror

import (
	"errors"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestApiError(t *testing.T) {
	expectedMsg := "test"
	expectedCode := 201
	expectedErr := "this is a test"
	a := apiError{
		ErrMsg:        expectedMsg,
		ErrStatusCode: expectedCode,
		Err:           &expectedErr,
	}

	require.Equal(t, expectedMsg, a.Message())
	require.Equal(t, expectedCode, a.StatusCode())
	require.Equal(t, &expectedErr, a.Error())
}

func TestNewBadRequestError(t *testing.T) {
	a := NewBadRequestError("test")
	require.Equal(t, http.StatusBadRequest, a.StatusCode())
}

func TestNewNotFoundError(t *testing.T) {
	a := NewNotFoundError("test")
	require.Equal(t, http.StatusNotFound, a.StatusCode())
}

func TestNewInternalServerError(t *testing.T) {
	a := NewInternalServerError("test", errors.New("test"))
	require.Equal(t, http.StatusInternalServerError, a.StatusCode())
}
