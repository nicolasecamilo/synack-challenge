package apierror

import "net/http"

type ApiError interface {
	Message() string
	StatusCode() int
	Error() string
}

type apiError struct {
	ErrMsg        string `json:"message"`
	ErrStatusCode int    `json:"status"`
	Err           string `json:"error,omitempty"`
}

func (a *apiError) Message() string {
	return a.ErrMsg
}

func (a *apiError) StatusCode() int {
	return a.ErrStatusCode
}

func (a *apiError) Error() string {
	return a.Err
}

func NewBadRequestError(msg string) ApiError {
	return &apiError{ErrMsg: msg, ErrStatusCode: http.StatusBadRequest}
}

func NewNotFoundError(msg string) ApiError {
	return &apiError{ErrMsg: msg, ErrStatusCode: http.StatusNotFound}
}

func NewInternalServerError(msg string, err error) ApiError {
	return &apiError{ErrMsg: msg, ErrStatusCode: http.StatusInternalServerError, Err: err.Error()}
}
