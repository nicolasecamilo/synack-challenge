package images

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/logger"
)

type Handler interface {
	Post(c *gin.Context)
	Get(c *gin.Context)
}

type handler struct {
	service Service
}

func NewHandler(service Service) Handler {
	return &handler{service: service}
}

func (h *handler) Post(c *gin.Context) {
	file, err := c.FormFile("image")
	if err != nil {
		logger.Error(err.Error())
		apierr := apierror.NewBadRequestError(err.Error())
		c.JSON(apierr.StatusCode(), apierr)
		return
	}

	fileBytes, apierr := readFile(file)
	if apierr != nil {
		c.JSON(apierr.StatusCode(), apierr)
		return
	}

	id, apierr := h.service.HandleUpload(c, fileBytes)
	if apierr != nil {
		c.JSON(apierr.StatusCode(), apierr)
		return
	}

	c.JSON(http.StatusAccepted, gin.H{"id": id})
}

func (h *handler) Get(c *gin.Context) {
	id := c.Param("id")

	img, apierr := h.service.GetImageData(c, id)
	if apierr != nil {
		c.JSON(apierr.StatusCode(), apierr)
		return
	}

	c.JSON(img.Status.Code, img)
}

func readFile(file *multipart.FileHeader) ([]byte, apierror.ApiError) {
	f, err := file.Open()
	defer f.Close()
	if err != nil {
		msg := "error reading the file"
		logger.Error(msg, zap.Error(err))
		return nil, apierror.NewInternalServerError(msg, err)
	}

	b, _ := ioutil.ReadAll(f)
	return b, nil
}
