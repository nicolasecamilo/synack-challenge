package images

import (
	"errors"
	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/require"
	"net/http"
	"synack_challenge/internal/domain"
	"testing"
)

func TestStoreDataShouldReturnSetErr(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	imageData := &domain.ImageInformation{
		Identifier:       "test",
		Format:           "jpeg",
		Dimensions:       domain.ImageDimension{},
		ColorComposition: domain.ImageColorComposition{},
		Status:           domain.ImageStatus{},
	}
	mock.Regexp().ExpectSet(imageData.Identifier, "(.*)", 0).SetErr(errors.New("test"))

	repo := NewRepository(redis)

	apierr := repo.StoreData(nil, imageData, domain.PROCESSING, false)

	require.Equal(t, http.StatusInternalServerError, apierr.StatusCode())
}

func TestStoreDataShouldReturnNoErr(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	imageData := &domain.ImageInformation{
		Identifier:       "test",
		Format:           "jpeg",
		Dimensions:       domain.ImageDimension{},
		ColorComposition: domain.ImageColorComposition{},
		Status:           domain.ImageStatus{},
	}
	mock.Regexp().ExpectSet(imageData.Identifier, "(.*)", 0).SetVal("")

	repo := NewRepository(redis)

	apierr := repo.StoreData(nil, imageData, domain.SUCCESS, true)

	require.Nil(t, apierr)
}

func TestGetImageInformationShouldReturnNotFound(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	mock.ExpectGet("test").RedisNil()

	repo := NewRepository(redis)

	img, apierr := repo.GetImageInformation(nil, "test")
	require.Nil(t, img)
	require.Equal(t, http.StatusNotFound, apierr.StatusCode())
}

func TestGetImageInformationShouldReturnInternalServerErr(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	mock.ExpectGet("test").SetErr(errors.New("test"))

	repo := NewRepository(redis)

	img, apierr := repo.GetImageInformation(nil, "test")
	require.Nil(t, img)
	require.Equal(t, http.StatusInternalServerError, apierr.StatusCode())
}

func TestGetImageInformationShouldReturnNoErr(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	imageData := &domain.ImageInformation{
		Identifier:       "test",
		Format:           "jpeg",
		Dimensions:       domain.ImageDimension{},
		ColorComposition: domain.ImageColorComposition{},
		Status:           domain.ImageStatus{},
	}
	imgStr, _ := imageData.ToString()
	mock.ExpectGet("test").SetVal(imgStr)

	repo := NewRepository(redis)

	img, apierr := repo.GetImageInformation(nil, "test")
	require.Nil(t, apierr)
	require.Equal(t, imageData, img)
}

func TestHandleStatusShouldLogWhenRedisIsNil(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	mock.ExpectGet("test").RedisNil()

	repo := NewRepository(redis)

	repo.HandleStatus(nil, domain.PROCESSING, "test", nil)
}

func TestHandleStatusShouldLogError(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	mock.ExpectGet("test").SetErr(errors.New("test"))

	repo := NewRepository(redis)

	repo.HandleStatus(nil, domain.PROCESSING, "test", nil)
}

func TestHandleStatusShouldSetANewStatus(t *testing.T) {
	redis, mock := redismock.NewClientMock()
	imageData := &domain.ImageInformation{
		Identifier:       "test",
		Format:           "jpeg",
		Dimensions:       domain.ImageDimension{},
		ColorComposition: domain.ImageColorComposition{},
		Status:           domain.ImageStatus{},
	}
	imgStr, _ := imageData.ToString()
	mock.ExpectGet("test").SetVal(imgStr)
	mock.Regexp().ExpectSet("test", "(.+)", 0).SetVal("")

	repo := NewRepository(redis)

	repo.HandleStatus(nil, domain.FAILURE, "test", &imageData.Identifier)
}
