package images

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/domain"
)

type RepositoryMock struct {
	mock.Mock
}

func (r *RepositoryMock) StoreData(ctx *gin.Context, image *domain.ImageInformation, status domain.Status, update bool) apierror.ApiError {
	args := r.Called(ctx, image, status, update)
	err := args.Get(0)
	if err == nil {
		return nil
	}
	return err.(apierror.ApiError)
}

func (r *RepositoryMock) GetImageInformation(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError) {
	args := r.Called(ctx, id)
	info := args.Get(0)
	err := args.Get(1)
	if err == nil {
		if info == nil {
			return nil, nil
		}
		return info.(*domain.ImageInformation), nil
	}
	if info == nil {
		return nil, err.(apierror.ApiError)
	}
	return info.(*domain.ImageInformation), err.(apierror.ApiError)
}

func (r *RepositoryMock) HandleStatus(ctx *gin.Context, status domain.Status, id string, reason *string) {
	r.Called(ctx, status, id, reason)
}
