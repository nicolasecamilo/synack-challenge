package images

import (
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"synack_challenge/internal/apierror"
	"testing"
)

func TestHandleUploadShouldReturnStoreDataError(t *testing.T) {
	expected := apierror.NewBadRequestError("test")
	repo := new(RepositoryMock)
	repo.On("StoreData", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(expected)

	svc := NewService(repo)

	f, err := os.Open("../../test/example/google.jpg")
	require.Nil(t, err)
	defer f.Close()
	fileBytes, _ := ioutil.ReadAll(f)

	_, apierr := svc.HandleUpload(nil, fileBytes)

	require.Equal(t, expected, apierr)
}

func TestHandleUploadShouldReturnNoError(t *testing.T) {
	repo := new(RepositoryMock)
	repo.On("StoreData", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	svc := NewService(repo)

	f, err := os.Open("../../test/example/google.jpg")
	require.Nil(t, err)
	defer f.Close()
	fileBytes, _ := ioutil.ReadAll(f)

	id, _ := svc.HandleUpload(nil, fileBytes)
	require.NotNil(t, id)
}
