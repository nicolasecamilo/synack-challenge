package images

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/jxskiss/base62"
	"go.uber.org/zap"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"math/rand"
	"strings"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/domain"
	"synack_challenge/internal/logger"
	"time"
)

type Service interface {
	HandleUpload(ctx *gin.Context, file []byte) (string, apierror.ApiError)
	GetImageData(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) Service {
	return &service{repository: repository}
}

func (s *service) HandleUpload(ctx *gin.Context, file []byte) (string, apierror.ApiError) {
	var apierr apierror.ApiError
	imageData := &domain.ImageInformation{}
	imageData.Identifier, apierr = generateID(file)
	if apierr != nil {
		return "", apierr
	}
	apierr = s.repository.StoreData(ctx, imageData, domain.PROCESSING, false)
	if apierr != nil {
		return "", apierr
	}

	go func() {
		img, apierr := getImageData(file, imageData)
		if apierr != nil {
			err := apierr.Error()
			s.repository.HandleStatus(ctx, domain.FAILURE, imageData.Identifier, &err)
			return
		}
		imageData.ColorComposition = countPixels(img)
		s.repository.StoreData(ctx, imageData, domain.SUCCESS, true)
	}()

	return imageData.Identifier, nil
}

func (s *service) GetImageData(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError) {
	return s.repository.GetImageInformation(ctx, id)
}

func generateID(file []byte) (string, apierror.ApiError) {
	encodedStr := base62.EncodeToString(file)
	rand.Seed(time.Now().UnixNano())
	var id strings.Builder
	for i := 0; i < 5; i++ {
		err := id.WriteByte(encodedStr[rand.Intn(len(encodedStr))])
		if err != nil {
			// tbh I don't why this could fail
			msg := "error writing builder"
			logger.Error(msg, zap.Error(err))
			return "", apierror.NewInternalServerError(msg, err)
		}
	}
	return id.String(), nil
}

func getImageData(file []byte, imageData *domain.ImageInformation) (image.Image, apierror.ApiError) {
	i, f, err := image.Decode(bytes.NewReader(file))
	if err != nil {
		msg := "error decoding image"
		logger.Error(msg, zap.Error(err))
		return nil, apierror.NewInternalServerError(msg, err)
	}
	imageData.Format = f
	imageData.Dimensions = domain.ImageDimension{
		Width:  i.Bounds().Dx(),
		Height: i.Bounds().Dy(),
	}
	return i, nil
}

func countPixels(img image.Image) domain.ImageColorComposition {
	colorComposition := domain.ImageColorComposition{}
	for i := 0; i < img.Bounds().Max.Y; i++ {
		for j := 0; j < img.Bounds().Max.X; j++ {
			colors := img.At(j, i)
			r, g, b, _ := colors.RGBA()
			if uint8(r) > 128 {
				colorComposition.Red++
			} else if uint8(g) > 128 {
				colorComposition.Green++
			} else if uint8(b) > 128 {
				colorComposition.Blue++
			}
		}
	}

	return colorComposition
}
