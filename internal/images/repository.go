package images

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	"net/http"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/domain"
	"synack_challenge/internal/logger"
)

const (
	statusSuffix = "_STATUS"
)

type Repository interface {
	StoreData(ctx *gin.Context, image *domain.ImageInformation, status domain.Status, update bool) apierror.ApiError
	GetImageInformation(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError)
	HandleStatus(ctx *gin.Context, status domain.Status, id string, reason *string)
}

type repository struct {
	redis *redis.Client
}

func NewRepository(redis *redis.Client) Repository {
	return &repository{redis: redis}
}

func (r *repository) StoreData(ctx *gin.Context, image *domain.ImageInformation, status domain.Status, update bool) apierror.ApiError {
	if !update {
		image.Status.Status = domain.PROCESSING
		image.Status.Code = http.StatusProcessing
	} else {
		image.Status.Status = status
		image.Status.SetStatusCode()
	}
	strData, apierr := image.ToString()
	if apierr != nil {
		return apierr
	}
	err := r.redis.Set(ctx, image.Identifier, strData, 0).Err()
	if err != nil {
		errmsg := "something happened trying to store an image"
		logger.Error(errmsg, zap.Error(err), zap.String("id", image.Identifier))
		return apierror.NewInternalServerError(errmsg, err)
	}

	return nil
}

func (r *repository) GetImageInformation(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError) {
	img, err := r.redis.Get(ctx, id).Result()
	if err != nil {
		if err == redis.Nil {
			msg := fmt.Sprintf("image %s not found", id)
			logger.Info(msg, zap.String("id", id))
			return nil, apierror.NewNotFoundError(msg)
		} else {
			msg := fmt.Sprintf("something happened fetching image id %s information", id)
			logger.Error(msg, zap.Error(err))
			return nil, apierror.NewInternalServerError(msg, err)
		}
	}

	return domain.StringToImageData(img)
}

func (r *repository) HandleStatus(ctx *gin.Context, status domain.Status, id string, reason *string) {
	var apierr apierror.ApiError
	img, err := r.redis.Get(ctx, id).Result()
	if err == redis.Nil {
		logger.Info(fmt.Sprintf("status for %s not found", id), zap.String("id", id))
		return
	} else if err != nil {
		logger.Error(fmt.Sprintf("something happened fetching image id %s information", id), zap.Error(err))
		return
	}

	imgData, apierr := domain.StringToImageData(img)
	if apierr != nil {
		return
	}

	imgData.Status.Status = status
	imgData.Status.SetStatusCode()
	if reason != nil {
		imgData.Status.Reason = reason
	}

	r.redis.Set(ctx, id, imgData, 0)
}
