package images

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"synack_challenge/internal/apierror"
	"synack_challenge/internal/domain"
)

type ServiceMock struct {
	mock.Mock
}

func (s *ServiceMock) HandleUpload(ctx *gin.Context, file []byte) (string, apierror.ApiError) {
	args := s.Called(ctx, file)
	str := args.String(0)
	err := args.Get(1)
	if err == nil {
		return str, nil
	}
	return str, err.(apierror.ApiError)
}

func (s *ServiceMock) GetImageData(ctx *gin.Context, id string) (*domain.ImageInformation, apierror.ApiError) {
	args := s.Called(ctx, id)
	info := args.Get(0)
	err := args.Get(1)
	if err == nil {
		if info == nil {
			return nil, nil
		}
		return info.(*domain.ImageInformation), nil
	}
	if info == nil {
		return nil, err.(apierror.ApiError)
	}
	return info.(*domain.ImageInformation), err.(apierror.ApiError)
}
