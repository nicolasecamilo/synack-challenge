package main

import "synack_challenge/internal/http"

func main() {
	if err := http.InitApp().Run(":8081"); err != nil {
		panic(err)
	}
}
