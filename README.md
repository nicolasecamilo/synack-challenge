# Read Me

## Usage

### Pre-requisites

- Docker
- Docker compose

You just have to simply run `docker compose up` and wait until the server is up and running :).

### API documentation

Once the server is up, `http://localhost:8082 has the swagger-ui that contains everything you need to know about the API.

Some cURLs:

#### POST Image
```curl
curl --location --request POST 'http://localhost:8080/images' \
--form 'image=@"/path/to/image"'
```

#### Get Image
```curl
curl --location --request GET 'http://localhost:8080/images/{id}'
```

## Design decisions

### ID

#### Why do I use 5 random characters from the base62 encoded image for ID generation?

5 random characters will grant 62^5 (916,132,832) unique IDs. I think for the sake of the challenge it will be enough, but if needed more characters could be used.

Storing almost 50k images per day will provide us enough IDs for almost 50 years. Adding just another character (which will be simple enough) it will grant us almost 3000 years if I'm not mistaken.

#### But why base62?

Shorter and readable strings than Base32, it contains only alphanumeric symbols

### Database

#### Why Redis?

I was actually planning on using Cassandra just because that's what lies behind DynamoDB, but I find using Redis simpler (and I just needed a key-value storage).

#### Why NoSQL over SQL?

- I think that I don't need any kind of relation between the data
- Depending on the use case usually SQL databases are more expensive than NoSQL (according to AWS pricing calculator, 1GB NoSQL DB will cost ~$18 USD vs $50 for the SQL DB -2 CPU 4GB machine-)
- Scaling will be easier
- Generally faster than SQL (https://www.cs.rochester.edu/courses/261/fall2017/termpaper/submissions/06/Paper.pdf)

### Code

#### init function in logger

Not a big fan of init function; I avoid whenever possible (in fact, I usually just use it for loggers because this will help with testing)

## Folder structure

- `cmd`: this is where the main application lives
- `internal`: private application code
  - `apierror`: this package is used to structure errors (might go into domain but I actually like the apierror package)
  - `database`: creates the redis database client
  - `http`: used for router creation and route mapping
  - `images`: domain oriented folder, has everything related to `/images` endpoint
  - `logger`: helps with logging
- `test`: integration tests

## Things I would have done different

Just because I didn't had enough free time for coding (been pretty busy this weekend); I would have had:

- Added integration tests, which is pretty simple
- Added handler tests

## Third-party libraries used

- go-redis (https://github.com/go-redis/redis)
- gin-gonic (https://github.com/gin-gonic/gin)
- jxskiss/base62 (https://github.com/jxskiss/base62)
- stretchr/testify (https://github.com/stretchr/testify)