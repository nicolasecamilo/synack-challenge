## Assignment

Create a dockerized golang microservice with two RESTful API operations:

•	upload an image file, responding with a unique identifier that can be used to query the second API operation for results

•	when queried with a known unique identifier, the response shall indicate that the identifier is for a file being processed, has failed to process for some reason, or for successful processing will indicate the file format, dimensions, and the separate count of Red, Green or Blue pixels, where the pixel has an R, G or B value greater than 128.

You may use any standard or third-party libraries you wish.

## Out-of-Scope
For this exercise, there is no need to

•	use a persistent data store

•	provide any AuthN or AuthZ mechanisms in the API

## Evaluation
Please return the submission as zipfile or a link to a publicly accessible code repository (github or gitea preferred).

The submission will be evaluated on the following criteria:

•	Demonstrably defect-free, working implementation of API operations with appropriate error handling

•	Unit tests and any other test code

•	Adherence to RESTful style with thoughtful use of HTTP Methods, HTTP Headers, URL schemes, and resource representation

•	Lightweight docker container adhering to security best practice

•	API documentation in OpenAPI 3 format

•	Appropriate documentation, identifying assumptions or ambiguities, instructions for use

•	Appropriate use of standard and third-party libraries if used